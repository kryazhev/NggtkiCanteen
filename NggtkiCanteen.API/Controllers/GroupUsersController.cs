﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NggtkiCanteen.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Controllers
{
    [Route("api/[controller]")]
    public class GroupUsersController : ControllerBase
    {

        private DataBaseContext dataBaseContext;
        private UserManager<User> userManager;

        public GroupUsersController(DataBaseContext dataBaseContext, UserManager<User> userManager)
        {
            this.dataBaseContext = dataBaseContext;
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<IEnumerable<GroupUsers>> All()
        {
            return await dataBaseContext.GroupUsers
                .Include(g => g.Groups)
                .Include(g => g.User)
                .ToListAsync();
        }

        [HttpPost]
        public async Task Create(string userId, int groupId)
        {
            GroupUsers groupUsers = new GroupUsers()
            {
                UserId = userId,
                GroupsId = groupId
            };
            await dataBaseContext.GroupUsers.AddAsync(groupUsers);
            await dataBaseContext.SaveChangesAsync();
        }

        [Authorize]
        [HttpGet("user")]
        public async Task<IEnumerable<string>> UserGroup()
        {
            User user = await userManager.GetUserAsync(User);
            List<string> groupUsers = await dataBaseContext.GroupUsers
                .Include(g => g.Groups)
                .Where(g => g.UserId == user.Id)
                .Select(g => g.Groups.Name).ToListAsync();

            if(groupUsers == null)
            {
                return new List<string> { };
            }

            return groupUsers;
        }
    }
}
