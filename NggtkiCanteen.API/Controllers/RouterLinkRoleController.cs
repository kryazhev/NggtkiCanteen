﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NggtkiCanteen.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouterLinkRoleController : ControllerBase
    {

        private readonly DataBaseContext dataBaseContext;

        public RouterLinkRoleController(DataBaseContext dataBaseContext)
        {
            this.dataBaseContext = dataBaseContext;
        }

        [HttpGet]
        public async Task<IEnumerable<RouterLinkRole>> All()
        {
            return await dataBaseContext.RouterLinkRoles.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<RouterLinkRole> Get(int id)
        {
            return await dataBaseContext.RouterLinkRoles.FindAsync(id);
        }

        [HttpPost]
        public async Task Post([FromBody] (string route, string role) value)
        {
            await dataBaseContext.RouterLinkRoles.AddAsync(new RouterLinkRole()
            {
                Route = value.route,
                Role = value.role
            });
            await dataBaseContext.SaveChangesAsync();
        }

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] (string route, string role) value)
        {
            RouterLinkRole routerLinkRole = await dataBaseContext.RouterLinkRoles.FindAsync(id);
            routerLinkRole.Route = value.route;
            routerLinkRole.Role = value.role;
            dataBaseContext.RouterLinkRoles.Update(routerLinkRole);
            await dataBaseContext.SaveChangesAsync();
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            RouterLinkRole routerLinkRole = await dataBaseContext.RouterLinkRoles.FindAsync(id);
            dataBaseContext.RouterLinkRoles.Remove(routerLinkRole);
            await dataBaseContext.SaveChangesAsync();
        }
    }
}
