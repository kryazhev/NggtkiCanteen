﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NggtkiCanteen.API.Models;
using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly DataBaseContext dataBaseContext;

        public GroupController(DataBaseContext dataBaseContext)
        {
            this.dataBaseContext = dataBaseContext;
        }

        [HttpGet]
        public async Task<IEnumerable<GroupResult>> Get()
        {
            List<Group> groups = await dataBaseContext.Groups
                .OrderBy(g => g.Name)
                .ToListAsync();
            List<GroupResult> resultGroup = new List<GroupResult>();
            foreach (Group group in groups)
            {
                var count = dataBaseContext.GroupUsers.Where(g => g.GroupsId == group.Id).ToList().Count;
                resultGroup.Add(new GroupResult()
                {
                    Id = group.Id,
                    Name = group.Name,
                    Count = count
                });
            }

            return resultGroup;
        }

        [HttpGet("users")]
        public async Task<IEnumerable<UserModel>> UsersByGroupId(int groupId)
        {
            return await dataBaseContext.GroupUsers
                .Include(g => g.User)
                .Where(g => g.GroupsId == groupId)
                .Select(g => new UserModel()
                {
                    Id = g.User.Id,
                    UserName = g.User.UserName,
                    Email = g.User.Email,
                    Phone = g.User.PhoneNumber
                })
                .ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<Group> GetById(int id)
        {
            return await dataBaseContext.Groups.FindAsync(id);
        }

        [HttpPost]
        public async Task<Group> CreateGroup([FromBody] GroupCreate groupCreate)
        {
            Group group = new Group() { Name = groupCreate.Name };
            await dataBaseContext.Groups.AddAsync(group);
            await dataBaseContext.SaveChangesAsync();
            return group;
        }

        [HttpDelete("{id}")]
        public async Task DeleteGroup(int id)
        {
            Group group = await dataBaseContext.Groups.FindAsync(id);
            dataBaseContext.Groups.Remove(group);
            await dataBaseContext.SaveChangesAsync();
        }
    }
}
