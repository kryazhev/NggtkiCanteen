﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Rest;
using NggtkiCanteen.API.Models;
using NggtkiCanteen.Core;
using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserManager<User> userManager;
        private DataBaseContext dataBaseContext;
        private SignInManager<User> signInManager;
        private IJwtGenerator<User> jwtGenerator;

        public UserController(
            UserManager<User> userManager,
            DataBaseContext dataBaseContext,
            SignInManager<User> signInManager,
            IJwtGenerator<User> jwtGenerator)
        {
            this.userManager = userManager;
            this.dataBaseContext = dataBaseContext;
            this.signInManager = signInManager;
            this.jwtGenerator = jwtGenerator;
        }

        [Authorize]
        [HttpGet]
        public async Task<IEnumerable<UserModel>> All()
        {
            List<UserModel> users = await dataBaseContext.Users.Select(user => new UserModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                Phone = user.PhoneNumber
            }).ToListAsync();
            return users;
        }


        [HttpGet("isGroups")]
        public async Task<IEnumerable<UserModel>> AllIsGroup()
        {
            List<User> users = await dataBaseContext.Users.ToListAsync();
            List<GroupUsers> groupUsers = await dataBaseContext.GroupUsers.ToListAsync();
            List<UserModel> userModels = new List<UserModel>();

            foreach (var user in users)
            {
                foreach (var group in groupUsers)
                {
                    if(user.Id == group.UserId)
                    {
                        userModels.Add(new UserModel() { 
                            Id = user.Id,
                            UserName = user.UserName,
                            Email = user.Email,
                            Phone = user.PhoneNumber
                        });
                    }
                }
            }

            return userModels;
        }

        [Authorize]
        [HttpGet("userIsGroup")]
        public async Task<bool> UserIsGroup()
        {
            User user = await userManager.GetUserAsync(User);
            GroupUsers groupUsers = dataBaseContext.GroupUsers
                .Where(g => g.UserId == user.Id)
                .FirstOrDefault();

            return groupUsers != null ? true : false;
        }

        [Authorize]
        [HttpGet("account")]
        public async Task<UserModel> Account()
        {            
            UserModel userModel = new UserModel();
            User user = await userManager.GetUserAsync(User);
            userModel.UserName = user?.UserName;
            userModel.Email = user?.Email;
            userModel.Phone = user?.PhoneNumber;
            return userModel;
        }

        [HttpPost]
        public async Task<User> Create([FromBody] CreateUser createUser)
        {
            User user = new User()
            {
                UserName = createUser.UserName,
                Email = createUser.Email
            };

            IdentityResult result = await userManager.CreateAsync(user, createUser.Password);

            if (result.Succeeded)
            {
                return user;
            }

            Console.WriteLine(result);

            throw new RestException(result.Errors.First().Description);
        }

        [HttpPost("signIn")]
        public async Task<UserResponse> SingIn([FromBody] SignInUser signInUser, CancellationToken cancellationToken)
        {
            User user = await userManager.FindByEmailAsync(signInUser.Email);

            if (user is null)
            {
                throw new RestException(HttpStatusCode.Unauthorized.ToString());
            }

            var result = await signInManager.CheckPasswordSignInAsync(user, signInUser.Password, false);

            if (result.Succeeded)
            {
                return new UserResponse
                {
                    Token = jwtGenerator.CreateToken(user),
                    Id = user.Id
                };
            }

            throw new RestException(HttpStatusCode.Unauthorized.ToString());

        }

        [Authorize]
        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePasswor([FromBody] ChangePasswordModel changePasswordModel)
        {
            User user = await userManager.FindByIdAsync(changePasswordModel.Id);

            var result = await userManager.ChangePasswordAsync(user, changePasswordModel.CurrentPassword, changePasswordModel.NewPassword);

            if (result.Succeeded)
            {
                return Ok("password change");
            }

            throw new RestException(HttpStatusCode.Unauthorized.ToString());
        }

        [Authorize]
        [HttpDelete]
        public async Task DeleteUser(string userid)
        {
            User user = await userManager.FindByIdAsync(userid);
            await userManager.DeleteAsync(user);
        }
    }
}
