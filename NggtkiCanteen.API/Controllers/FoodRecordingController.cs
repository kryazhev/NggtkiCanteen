﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NggtkiCanteen.API.Models;
using NggtkiCanteen.Core.Models;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;



namespace NggtkiCanteen.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodRecordingController : ControllerBase
    {
        private readonly DataBaseContext dataBaseContext;
        private readonly UserManager<User> userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public FoodRecordingController(DataBaseContext dataBaseContext, 
                                        UserManager<User> userManager,
                                        IWebHostEnvironment _hostingEnvironment)
        {
            this.dataBaseContext = dataBaseContext;
            this.userManager = userManager;
            this._hostingEnvironment = _hostingEnvironment;
        }


        [HttpGet]
        public async Task<IEnumerable<FoodRecordingResult>> Get()
        {
            List<FoodRecordingResult> foodRecordingResults = new List<FoodRecordingResult>();

            var results = await (from foodRecording in dataBaseContext.FoodRecordings
                                   select new {
                                       foodRecording.FoodTimeId,
                                       foodRecording.Date
                                   })
                                   
                                   .Distinct()
                                   .ToListAsync();

            foreach (var result in results)
            {
                FoodRecordingResult foodRecordingResult = new FoodRecordingResult();
                foodRecordingResult.GroupFoodRecordings = new List<GroupFoodRecording>();

                foodRecordingResult.FoodTime = await dataBaseContext.FoodTimes.FirstOrDefaultAsync(f => f.Id == result.FoodTimeId);
                foodRecordingResult.Date = result.Date;

                var groups = await (from groupUsers in dataBaseContext.GroupUsers
                                                    join foodrecording in dataBaseContext.FoodRecordings on groupUsers.UserId equals foodrecording.UserId
                                                    where foodrecording.FoodTimeId == result.FoodTimeId
                                                    select groupUsers.Groups)
                                                    .Distinct()
                                                    .ToListAsync();

                foreach (var group in groups)
                {
                    var users = await (from user in dataBaseContext.Users
                                       join groupUser in dataBaseContext.GroupUsers on user.Id equals groupUser.UserId
                                       join foodRecording in dataBaseContext.FoodRecordings on user.Id equals foodRecording.UserId
                                       join foodTime in dataBaseContext.FoodTimes on foodRecording.FoodTimeId equals foodTime.Id
                                       select new { groupUser, foodRecording, foodTime })
                                       .Where(u => u.groupUser.GroupsId == group.Id 
                                                && u.foodRecording.Date == result.Date
                                                && u.foodTime.Id == result.FoodTimeId)
                                       .ToListAsync();

                    GroupFoodRecording groupFoodRecording = new GroupFoodRecording();
                    groupFoodRecording.Group = group;
                    groupFoodRecording.Count = users.Count();
                    foodRecordingResult.GroupFoodRecordings.Add(groupFoodRecording);
                }
               

                foodRecordingResults.Add(foodRecordingResult);
            }

            return foodRecordingResults;
            
        }

        [HttpGet("user/{userId}")]
        public async Task<IEnumerable<FoodRecording>> FoodRecordingsUser(string userId)
        {
            List<FoodRecording> foodRecordings = await dataBaseContext.FoodRecordings
                .Where(g => g.UserId == userId)
                .Include(g => g.User)
                .Include(g => g.FoodTime)
                .ToListAsync();
            return foodRecordings;
        }

        [HttpPost()]
        public async Task<FoodRecording> CreateFoodRecordingByID(string userId,[FromBody] FoodRecordingsCreate foodRecordingsCreate)
        {
            try
            {
                User user = await userManager.FindByIdAsync(userId);
                FoodTime foodTime = await dataBaseContext.FoodTimes.FirstOrDefaultAsync(f => f.Id == foodRecordingsCreate.FoodTimeId);
                FoodRecording foodRecording = new FoodRecording()
                {
                    UserId = user.Id,
                    FoodTimeId = foodTime.Id,
                    Date = foodRecordingsCreate.Date
                };
                await dataBaseContext.FoodRecordings.AddAsync(foodRecording);
                await dataBaseContext.SaveChangesAsync();
                return foodRecording;
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            FoodRecording foodRecording = await dataBaseContext.FoodRecordings.FindAsync(id);
            dataBaseContext.Remove(foodRecording);
            await dataBaseContext.SaveChangesAsync();
        }

        [HttpGet("download")]
        public async Task<IActionResult> ToExcel()
        {
            List<FoodRecording> listRecordings = await dataBaseContext.FoodRecordings
                .Include(f => f.User)
                .Include(f => f.FoodTime)
                .ToListAsync();

            string sFileName = @"Отчёт.xlsx";
            var memory = new MemoryStream();
            using (var fs = new FileStream(sFileName, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("employee");

                for (int i = 0; i < listRecordings.Count(); i++)
                {
                    IRow row = excelSheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(listRecordings[i].FoodTime.Name);
                    row.CreateCell(1).SetCellValue(listRecordings[i].User.UserName);
                    row.CreateCell(2).SetCellValue(listRecordings[i].Date.ToString());
                }

                workbook.Write(fs);
            }
            using (var stream = new FileStream(sFileName, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }
    }
}
