﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NggtkiCanteen.API.Models;
using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private RoleManager<IdentityRole> roleManager;
        private UserManager<User> userManager;

        public RoleController(
            RoleManager<IdentityRole> roleManager,
            UserManager<User> userManager
            ) {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<List<IdentityRole>> AllAsync()
        {
            return await roleManager.Roles.ToListAsync();
        }
        [Authorize]
        [HttpGet("userRoles")]
        public async Task<IList<string>> UserRoles()
        {
            User user = await userManager.GetUserAsync(User);
            return await userManager.GetRolesAsync(user);
        }

        [Authorize]
        [HttpPost("addUserRole")]
        public async Task<IList<string>> AddUserRole([FromBody] AddRoleUser addRoleUser)
        {
            try
            {
                User user = await userManager.FindByIdAsync(addRoleUser.Id);
                await userManager.AddToRoleAsync(user, addRoleUser.Role);
                return await userManager.GetRolesAsync(user);
            } catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(string name)
        {
            IdentityResult result = await roleManager.CreateAsync(new IdentityRole(name));
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

    }
}
