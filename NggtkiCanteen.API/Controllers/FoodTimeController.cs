﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NggtkiCanteen.API.Models;
using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodTimeController : ControllerBase
    {
        public readonly DataBaseContext dataBaseContext;

        public FoodTimeController(DataBaseContext dataBaseContext)
        {
            this.dataBaseContext = dataBaseContext;
        }

        // GET: api/<FoodTimeController>
        [HttpGet]
        public async Task<IEnumerable<FoodTime>> All()
        {
            List<FoodTime> foodTimes = await dataBaseContext.FoodTimes.ToListAsync();

            return foodTimes;
        }

        // GET api/<FoodTimeController>/5
        [HttpGet("{id}")]
        public async Task<FoodTime> Get(int id)
        {
            try
            {
                return await dataBaseContext.FoodTimes.FindAsync(id);
            } catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        // POST api/<FoodTimeController>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] FoodTimeCreate foodTimeCreate)
        {
            try
            {
                FoodTime foodTime = new FoodTime() { 
                    Name = foodTimeCreate.Name,
                    Time = new TimeSpan(foodTimeCreate.Hour,foodTimeCreate.Minutes,0)
                };
                await dataBaseContext.FoodTimes.AddAsync(foodTime);
                int countResult = dataBaseContext.SaveChanges();

                if (countResult > 0)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }

        }

        // PUT api/<FoodTimeController>/5
        [HttpPut("{id}")]
        public async Task<FoodTime> Put(int id, [FromBody] FoodTimeParameter foodTimeParameter)
        {
            try
            {
                FoodTime foodTime = await dataBaseContext.FoodTimes.FindAsync(id);
                foodTime.Name = foodTimeParameter.Name;
                dataBaseContext.FoodTimes.Update(foodTime);
                dataBaseContext.SaveChanges();
                return foodTime;
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        // DELETE api/<FoodTimeController>/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                FoodTime foodTime = await dataBaseContext.FoodTimes.FindAsync(id);
                dataBaseContext.FoodTimes.Remove(foodTime);
                int CountResult = dataBaseContext.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                Response.StatusCode = 404;
                return NotFound(e.Message);
            }
        }
    }
}
