﻿using NggtkiCanteen.API.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API
{
    public class FoodCompearer : IEqualityComparer<Food>
    {
        public bool Equals(Food x, Food y)
        {
            if (Object.ReferenceEquals(x, y)) return true;

            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            return x.FoodRecording.Date == y.FoodRecording.Date;
        }

        public int GetHashCode([DisallowNull] Food obj)
        {
            throw new NotImplementedException();
        }
    }
}
