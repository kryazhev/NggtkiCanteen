﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NggtkiCanteen.API.Migrations
{
    public partial class addkeysFoodTime1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                schema: "dbo",
                table: "FoodRecordings",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FoodTimeId",
                schema: "dbo",
                table: "FoodRecordings",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FoodRecordings_FoodTimeId",
                schema: "dbo",
                table: "FoodRecordings",
                column: "FoodTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodRecordings_UserId",
                schema: "dbo",
                table: "FoodRecordings",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRecordings_FoodTimes_FoodTimeId",
                schema: "dbo",
                table: "FoodRecordings",
                column: "FoodTimeId",
                principalSchema: "dbo",
                principalTable: "FoodTimes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRecordings_User_UserId",
                schema: "dbo",
                table: "FoodRecordings",
                column: "UserId",
                principalSchema: "dbo",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodRecordings_FoodTimes_FoodTimeId",
                schema: "dbo",
                table: "FoodRecordings");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodRecordings_User_UserId",
                schema: "dbo",
                table: "FoodRecordings");

            migrationBuilder.DropIndex(
                name: "IX_FoodRecordings_FoodTimeId",
                schema: "dbo",
                table: "FoodRecordings");

            migrationBuilder.DropIndex(
                name: "IX_FoodRecordings_UserId",
                schema: "dbo",
                table: "FoodRecordings");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                schema: "dbo",
                table: "FoodRecordings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FoodTimeId",
                schema: "dbo",
                table: "FoodRecordings",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}
