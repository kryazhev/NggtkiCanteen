﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NggtkiCanteen.API.Migrations
{
    public partial class newmodelGroupUserskeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "GroupId",
                schema: "dbo",
                table: "GroupUsers",
                newName: "GroupsId");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                schema: "dbo",
                table: "GroupUsers",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GroupUsers_GroupsId",
                schema: "dbo",
                table: "GroupUsers",
                column: "GroupsId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupUsers_UserId",
                schema: "dbo",
                table: "GroupUsers",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupUsers_Groups_GroupsId",
                schema: "dbo",
                table: "GroupUsers",
                column: "GroupsId",
                principalSchema: "dbo",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupUsers_User_UserId",
                schema: "dbo",
                table: "GroupUsers",
                column: "UserId",
                principalSchema: "dbo",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupUsers_Groups_GroupsId",
                schema: "dbo",
                table: "GroupUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupUsers_User_UserId",
                schema: "dbo",
                table: "GroupUsers");

            migrationBuilder.DropIndex(
                name: "IX_GroupUsers_GroupsId",
                schema: "dbo",
                table: "GroupUsers");

            migrationBuilder.DropIndex(
                name: "IX_GroupUsers_UserId",
                schema: "dbo",
                table: "GroupUsers");

            migrationBuilder.RenameColumn(
                name: "GroupsId",
                schema: "dbo",
                table: "GroupUsers",
                newName: "GroupId");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                schema: "dbo",
                table: "GroupUsers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);
        }
    }
}
