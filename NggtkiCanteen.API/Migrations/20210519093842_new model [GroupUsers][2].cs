﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NggtkiCanteen.API.Migrations
{
    public partial class newmodelGroupUsers2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupUsers_Groups_GroupId",
                schema: "dbo",
                table: "GroupUsers");

            migrationBuilder.RenameColumn(
                name: "GroupId",
                schema: "dbo",
                table: "GroupUsers",
                newName: "GroupsId");

            migrationBuilder.RenameIndex(
                name: "IX_GroupUsers_GroupId",
                schema: "dbo",
                table: "GroupUsers",
                newName: "IX_GroupUsers_GroupsId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupUsers_Groups_GroupsId",
                schema: "dbo",
                table: "GroupUsers",
                column: "GroupsId",
                principalSchema: "dbo",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupUsers_Groups_GroupsId",
                schema: "dbo",
                table: "GroupUsers");

            migrationBuilder.RenameColumn(
                name: "GroupsId",
                schema: "dbo",
                table: "GroupUsers",
                newName: "GroupId");

            migrationBuilder.RenameIndex(
                name: "IX_GroupUsers_GroupsId",
                schema: "dbo",
                table: "GroupUsers",
                newName: "IX_GroupUsers_GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupUsers_Groups_GroupId",
                schema: "dbo",
                table: "GroupUsers",
                column: "GroupId",
                principalSchema: "dbo",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
