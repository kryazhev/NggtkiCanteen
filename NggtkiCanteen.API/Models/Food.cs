﻿using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class Food
    {
        public FoodTime FoodTime { get; set; }
        public FoodRecording FoodRecording { get; set; }
    }
}
