﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class FoodTimeResponse
    {
        public int Id { get;set; }
        public string Name { get; set; }
        public string Time { get; set; }
    }
}
