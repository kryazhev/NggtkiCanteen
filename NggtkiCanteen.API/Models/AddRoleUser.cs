﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class AddRoleUser
    {
        public string Id { get; set; }
        public string Role { get; set; }
    }
}
