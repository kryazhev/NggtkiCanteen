﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class FoodTimeCreate
    {
        public string Name { get; set; }
        public int Hour { get; set; }
        public int Minutes { get; set; }
    }
}
