﻿using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class GroupFoodRecording
    {
        public Group Group { get; set; }
        public int Count { get; set; }
    }
}
