﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class UserResponse
    {
        public string Token { get; set; }
        public string Id { get; set; }
    }
}
