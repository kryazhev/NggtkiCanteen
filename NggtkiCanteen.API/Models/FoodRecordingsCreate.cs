﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class FoodRecordingsCreate
    {
        public int FoodTimeId { get; set; }
        public DateTime Date { get; set; }
    }
}
