﻿using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.API.Models
{
    public class FoodRecordingResult
    {
        public FoodTime FoodTime { get; set; }
        public List<GroupFoodRecording> GroupFoodRecordings { get; set; }
        public DateTime Date { get; set; }
    }
}
