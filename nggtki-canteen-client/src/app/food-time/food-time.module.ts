import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodTimeRoutingModule } from './food-time-routing.module';
import { IndexComponent } from './index/index.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [IndexComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FoodTimeRoutingModule,
  ],
})
export class FoodTimeModule {}
