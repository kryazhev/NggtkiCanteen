import { TestBed } from '@angular/core/testing';

import { FoodTimeService } from './food-time.service';

describe('FoodTimeService', () => {
  let service: FoodTimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FoodTimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
