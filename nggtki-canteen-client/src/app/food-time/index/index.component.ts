import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { FoodTimeCreate } from 'src/app/models/FoodTimeCreate';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { FoodTimeService } from 'src/app/services/food-time.service';
import { GlobalService } from 'src/app/services/global.service';
import { GroupService } from 'src/app/services/group.service';
import { RoleService } from 'src/app/services/role.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  foodTimes: any;
  isModal = false;
  isLoader = true;
  createFoodTimeForm: FormGroup;
  isAdmin = false;

  constructor(
    private foodTimeService: FoodTimeService,
    private formBuilder: FormBuilder,
    private role: RoleService,
    private authorization: AuthorizationService,
    private group: GroupService,
    private global: GlobalService
  ) {
    this.createFoodTimeForm = this.formBuilder.group({
      name: new FormControl(null, Validators.required),
      hour: new FormControl(null, Validators.required),
      minutes: new FormControl(null, Validators.required),
    });
  }

  loadFoodTime(): void {
    this.foodTimeService.getFoodTimes().subscribe((data: any) => {
      this.foodTimes = data;
      this.isLoader = false;
    });
  }

  ngOnInit(): void {
    if (this.authorization.getToken() != null) {
      this.role.getUserRoles().subscribe((data: any) => {
        if (data.includes(this.role.adminRoleName)) {
          this.isAdmin = true;
        }
      });

      this.group.getGroupUser().subscribe((data: any) => {
        this.isAdmin = data.includes(this.global.ADMIN_KEY);
      });
    }
    this.loadFoodTime();
  }

  createFoodTime(): void {
    this.foodTimeService
      .createFoodTime(
        new FoodTimeCreate(
          this.createFoodTimeForm.get('name')?.value,
          this.createFoodTimeForm.get('hour')?.value,
          this.createFoodTimeForm.get('minutes')?.value
        )
      )
      .subscribe((data) => {
        this.loadFoodTime();
        this.createFoodTimeForm.clearAsyncValidators();
        this.isModal = false;
      });
  }

  deleteFoodTime(id: any): void {
    this.foodTimeService.deleteFoodTime(id).subscribe((data) => {
      this.loadFoodTime();
    });
  }
}
