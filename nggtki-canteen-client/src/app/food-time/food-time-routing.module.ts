import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizationGuard } from '../authorization/AuthorizationGuard';
import { IndexComponent } from './index/index.component';

export const FoodTimeRoutes: Routes = [
  {
    path: 'foodTime',
    component: IndexComponent,
    canActivate: [AuthorizationGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(FoodTimeRoutes)],
  exports: [RouterModule],
})
export class FoodTimeRoutingModule {}
