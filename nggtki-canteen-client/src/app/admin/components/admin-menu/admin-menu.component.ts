import { Component, OnInit } from '@angular/core';
import { AdminMenuItem } from 'src/app/models/AdminMenuItem';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss'],
})
export class AdminMenuComponent implements OnInit {
  adminMenuItems: AdminMenuItem[] = [];

  constructor(private global: GlobalService) {
    this.adminMenuItems = global.AdminMenuItems;
  }

  ngOnInit(): void { }
}
