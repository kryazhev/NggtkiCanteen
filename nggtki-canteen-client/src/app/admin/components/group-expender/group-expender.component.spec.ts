import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupExpenderComponent } from './group-expender.component';

describe('GroupExpenderComponent', () => {
  let component: GroupExpenderComponent;
  let fixture: ComponentFixture<GroupExpenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupExpenderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupExpenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
