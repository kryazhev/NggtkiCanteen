import { Component, Input, OnInit } from '@angular/core';
import { Group } from 'src/app/models/Group';
import { User } from 'src/app/models/User';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'app-group-expender',
  templateUrl: './group-expender.component.html',
  styleUrls: ['./group-expender.component.scss'],
})
export class GroupExpenderComponent implements OnInit {
  @Input() group?: Group;
  users: User[] = [];

  constructor(
    private auth: AuthorizationService,
    private groupService: GroupService
  ) {}

  ngOnInit(): void {
    this.groupService
      .getGroupUsers(this.group?.id)
      .subscribe((data: any) => (this.users = data));
  }
}
