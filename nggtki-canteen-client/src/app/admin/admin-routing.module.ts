import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../authorization/AdminGuard';
import { AuthorizationGuard } from '../authorization/AuthorizationGuard';
import { FoodRecordingComponent } from './views/food-recording/food-recording.component';
import { FoodTimesComponent } from './views/food-times/food-times.component';
import { GroupsComponent } from './views/groups/groups.component';
import { IndexComponent } from './views/index/index.component';
import { UsersComponent } from './views/users/users.component';

export const AdminRoutes: Routes = [
  {
    path: 'admin',
    component: IndexComponent,
    canActivate: [AuthorizationGuard],
    children: [
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'foodTimes',
        component: FoodTimesComponent,
      },
      {
        path: 'foodRecordings',
        component: FoodRecordingComponent,
      },
      {
        path: 'groups',
        component: GroupsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(AdminRoutes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
