import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { IndexComponent } from './views/index/index.component';
import { UsersComponent } from './views/users/users.component';
import { AdminMenuComponent } from './components/admin-menu/admin-menu.component';
import { FoodTimesComponent } from './views/food-times/food-times.component';
import { FoodRecordingComponent } from './views/food-recording/food-recording.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppFormsModule } from '../app-forms/app-forms.module';
import { GroupsComponent } from './views/groups/groups.component';
import { GroupExpenderComponent } from './components/group-expender/group-expender.component';

@NgModule({
  declarations: [
    IndexComponent,
    UsersComponent,
    AdminMenuComponent,
    FoodTimesComponent,
    FoodRecordingComponent,
    GroupsComponent,
    GroupExpenderComponent,
  ],
  exports: [AdminMenuComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppFormsModule,
  ],
})
export class AdminModule {}
