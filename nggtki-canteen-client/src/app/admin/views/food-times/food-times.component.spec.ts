import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodTimesComponent } from './food-times.component';

describe('FoodTimesComponent', () => {
  let component: FoodTimesComponent;
  let fixture: ComponentFixture<FoodTimesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodTimesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodTimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
