import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { FoodTime } from 'src/app/models/FoodTime';
import { FoodTimeCreate } from 'src/app/models/FoodTimeCreate';
import { FoodTimeService } from 'src/app/services/food-time.service';

@Component({
  selector: 'app-food-times',
  templateUrl: './food-times.component.html',
  styleUrls: ['./food-times.component.scss'],
})
export class FoodTimesComponent implements OnInit {
  foodTimes: FoodTime[] = [];
  isModal = false;
  createFoodTimeForm: FormGroup;

  constructor(
    private foodTimeService: FoodTimeService,
    private formBuilder: FormBuilder
  ) {
    this.createFoodTimeForm = this.formBuilder.group({
      name: new FormControl(null, Validators.required),
      hour: new FormControl(null, Validators.required),
      minutes: new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
    this.loadFoodTime();
  }

  createFoodTime(): void {
    this.foodTimeService
      .createFoodTime(
        new FoodTimeCreate(
          this.createFoodTimeForm.get('name')?.value,
          this.createFoodTimeForm.get('hour')?.value,
          this.createFoodTimeForm.get('minutes')?.value
        )
      )
      .subscribe((data) => {
        this.loadFoodTime();
        this.createFoodTimeForm.clearAsyncValidators();
        this.isModal = false;
      });
  }

  loadFoodTime() {
    this.foodTimeService.getFoodTimes().subscribe((data: any) => {
      this.foodTimes = data;
    });
  }

  deleteFoodTime(id: any): void {
    this.foodTimeService.deleteFoodTime(id).subscribe((data) => {
      this.loadFoodTime();
    });
  }
}
