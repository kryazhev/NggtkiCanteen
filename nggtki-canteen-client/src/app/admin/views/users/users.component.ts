import { Component, OnInit } from '@angular/core';
import { RegisterUser } from 'src/app/models/RegisterUser';
import { User } from 'src/app/models/User';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  isModal = false;

  constructor(private authorization: AuthorizationService) {}

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.authorization.getUsers().subscribe((data: any) => {
      this.users = data;
    });
  }

  register(user: RegisterUser) {
    this.authorization.register(user).subscribe((data) => {
      this.loadUsers();
      this.isModal = false;
    });
  }

  deleteUser(id: any) {
    this.authorization.deleteUser(id).subscribe(() => {
      this.loadUsers();
      alert('Пользователь удалён');
    });
  }
}
