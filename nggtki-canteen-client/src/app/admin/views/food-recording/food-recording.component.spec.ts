import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodRecordingComponent } from './food-recording.component';

describe('FoodRecordingComponent', () => {
  let component: FoodRecordingComponent;
  let fixture: ComponentFixture<FoodRecordingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodRecordingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodRecordingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
