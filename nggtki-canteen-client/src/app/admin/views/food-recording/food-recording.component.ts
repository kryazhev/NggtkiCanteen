import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { FoodRecordingCreate } from 'src/app/models/FoodRecordingCreate';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { FoodRecprdingsService } from 'src/app/services/food-recprdings.service';
import { FoodTimeService } from 'src/app/services/food-time.service';

export class FoodRecordingGroup {
  Name: string = '';
  users: any[] = [];
}

@Component({
  selector: 'app-food-recording',
  templateUrl: './food-recording.component.html',
  styleUrls: ['./food-recording.component.scss'],
})
export class FoodRecordingComponent implements OnInit {
  foodRecordings: any[] = [];
  isModal = false;
  foodRecordingForm: FormGroup;

  foodTimes: any[] = [];
  users: any[] = [];

  constructor(
    private foodRecording: FoodRecprdingsService,
    private formBuilder: FormBuilder,
    private foodTime: FoodTimeService,
    private auth: AuthorizationService
  ) {
    this.foodRecordingForm = this.formBuilder.group({
      user: new FormControl(null, Validators.required),
      foodTime: new FormControl('Выберите приём пищи', Validators.required),
      date: new FormControl(null, Validators.required),
    });
  }

  loadFoodRecording() {
    this.foodRecording.getFoodRecordingsAll().subscribe((data: any) => {
      this.foodRecordings = data;
    });
  }

  ngOnInit(): void {
    this.loadFoodRecording();

    this.foodTime.getFoodTimes().subscribe((data: any) => {
      this.foodTimes = data;
    });

    this.auth.getUsersIsGroup().subscribe((data: any) => (this.users = data));
  }

  createFoodRecording() {
    if (this.auth.getId() != null) {
      this.foodRecording
        .createFoodRecordings(
          new FoodRecordingCreate(
            this.foodRecordingForm.get('foodTime')?.value,
            this.foodRecordingForm.get('user')?.value,
            this.foodRecordingForm.get('date')?.value
          )
        )
        .subscribe((data: any) => {
          this.loadFoodRecording();
          this.isModal = false;
          alert('Вы записались');
        });
    }
  }

  deleteFoodRecording(id: any) {
    this.foodRecording.deleteFoodRecording(id).subscribe(() => {
      this.loadFoodRecording();
    });
  }
}
