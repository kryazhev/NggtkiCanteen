import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Group } from 'src/app/models/Group';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
})
export class GroupsComponent implements OnInit {
  groups: Group[] = [];
  users: any[] = [];
  createFormGroup: FormGroup;
  formAddGroupUser: FormGroup;
  isModalCreate = false;
  isModalAddUserGroup = false;
  selectedGroup = 0;

  constructor(
    private groupService: GroupService,
    private formBuilder: FormBuilder,
    private auth: AuthorizationService
  ) {
    this.createFormGroup = this.formBuilder.group({
      name: new FormControl(null, Validators.required),
    });

    this.formAddGroupUser = this.formBuilder.group({
      user: new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
    this.loadGroups();
    this.auth.getUsers().subscribe((data: any) => (this.users = data));
  }

  loadGroups(): void {
    this.groupService
      .getGroups()
      .subscribe((data: Group[]) => (this.groups = data));
  }

  delete(id: any): void {
    this.groupService.deleteGroup(id).subscribe(() => {
      this.loadGroups();
    });
  }

  createGroup() {
    this.groupService
      .createGroup(this.createFormGroup.get('name')?.value)
      .subscribe((data: any) => {
        this.isModalCreate = false;
        this.loadGroups();
      });
  }

  enableModalAddUserGroup(idGroup: any) {
    this.selectedGroup = idGroup;
    this.isModalAddUserGroup = true;
  }

  addGroupUser() {
    this.groupService
      .addGroupUser(
        this.formAddGroupUser.get('user')?.value,
        this.selectedGroup
      )
      .subscribe(() => {
        this.isModalAddUserGroup = false;
        this.selectedGroup = 0;
        this.formAddGroupUser.setValue({ user: null });
        this.loadGroups();
      });
  }
}
