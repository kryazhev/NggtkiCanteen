export class RegisterUser {
  constructor(
    public userName: string,
    public email: string,
    public password: string
  ) {}
}
