export class FoodTimeCreate {
  constructor(
    public name: string,
    public hour: string,
    public minutes: string
  ) {}
}
