export class FoodRecordingCreate {
  constructor(
    public foodTimeId?: string,
    public userId?: string,
    public date?: string
  ) {}
}
