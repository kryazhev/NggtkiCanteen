import { Time } from '@angular/common';

export class FoodTime {
  constructor(public id?: number, public name?: string, public time?: Time) {}
}
