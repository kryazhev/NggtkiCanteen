import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const AuthorizationRoutes: Routes = [
  {
    path: 'authorization',
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(AuthorizationRoutes)],
  exports: [RouterModule],
})
export class AuthorizationRoutingModule {}
