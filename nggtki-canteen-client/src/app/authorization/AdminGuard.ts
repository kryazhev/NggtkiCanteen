import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { retry } from 'rxjs/operators';
import { AuthorizationService } from '../services/authorization.service';
import { GlobalService } from '../services/global.service';
import { GroupService } from '../services/group.service';
import { RoleService } from '../services/role.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private group: GroupService,
    private role: RoleService,
    private global: GlobalService
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let isAdmin = true;

    this.role.getUserRoles().subscribe((data: any) => {
      if (data.includes(this.role.adminRoleName) == false) {
        isAdmin = false;
        this.router.navigateByUrl('/');
      }
    });

    this.group.getGroupUser().subscribe((data: any) => {
      if (data.includes(this.global.ADMIN_KEY) == false) {
        isAdmin = false;
        this.router.navigateByUrl('/');
      }
    });

    console.log(isAdmin);
    return false;
  }
}
