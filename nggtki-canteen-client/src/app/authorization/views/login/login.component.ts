import { Component, OnInit } from '@angular/core';
import { LoginUser } from 'src/app/models/LoginUser';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authorization: AuthorizationService) { }

  ngOnInit(): void {
  }

  login(user: LoginUser){
    this.authorization.login(user);
  }
}
