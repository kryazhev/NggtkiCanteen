import { Component, OnInit } from '@angular/core';
import { RegisterUser } from 'src/app/models/RegisterUser';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-auth-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(private authorization: AuthorizationService) {}

  ngOnInit(): void {}

  register(user: RegisterUser) {
    this.authorization.register(user);
  }
}
