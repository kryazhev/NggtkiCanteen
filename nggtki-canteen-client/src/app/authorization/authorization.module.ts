import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorizationRoutingModule } from './authorization-routing.module';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { ComponentsModule } from '../components/components.module';
import { AppFormsModule } from '../app-forms/app-forms.module';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    CommonModule,
    AuthorizationRoutingModule,
    ComponentsModule,
    AppFormsModule

  ],
})
export class AuthorizationModule { }
