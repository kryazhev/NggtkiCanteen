import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { GlobalService } from 'src/app/services/global.service';
import { GroupService } from 'src/app/services/group.service';
import { RoleService } from 'src/app/services/role.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  enabledMobile = false;
  isAdmin = false;

  constructor(
    public authorizationService: AuthorizationService,
    private role: RoleService,
    private global: GlobalService,
    private group: GroupService
  ) {}

  ngOnInit(): void {
    if (this.authorizationService.isAuthorization()) {
      this.role.getUserRoles().subscribe((data: any) => {
        this.isAdmin = data.includes(this.role.adminRoleName);
      });

      this.group.getGroupUser().subscribe((data: any) => {
        this.isAdmin = data.includes(this.global.ADMIN_KEY);
      });
    }
  }
}
