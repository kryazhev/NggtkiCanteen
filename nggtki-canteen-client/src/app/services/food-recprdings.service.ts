import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FoodRecordingCreate } from '../models/FoodRecordingCreate';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class FoodRecprdingsService {
  private API_ROOT: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private authorization: AuthorizationService
  ) {}

  getFoodRecordingsAll() {
    return this.http.get(`${this.API_ROOT}FoodRecording`);
  }

  getFoodRecordings() {
    return this.http.get(
      `${this.API_ROOT}FoodRecording/user/${this.authorization.getId()}`
    );
  }

  createFoodRecordings(foodRecording: FoodRecordingCreate) {
    return this.http.post(
      `${this.API_ROOT}FoodRecording?userId=${foodRecording.userId}`,
      {
        foodTimeId: foodRecording.foodTimeId,
        date: foodRecording.date,
      }
    );
  }

  deleteFoodRecording(id: any) {
    return this.http.delete(`${this.API_ROOT}FoodRecording/${id}`);
  }
}
