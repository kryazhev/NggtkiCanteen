import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  private API_ROOT: string = environment.apiUrl;
  public adminRoleName = 'admin';

  constructor(
    private http: HttpClient,
    private authorization: AuthorizationService
  ) {}

  getUserRoles() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.authorization.getToken()}`,
      }),
    };
    return this.http.get(`${this.API_ROOT}Role/userRoles`, httpOptions);
  }
}
