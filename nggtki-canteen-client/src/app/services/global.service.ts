import { Injectable } from '@angular/core';
import { AdminMenuItem } from '../models/AdminMenuItem';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  FIXED_NAVBAR_KEY = 'fixed_navbar';
  ADMIN_KEY = 'admin';

  AdminMenuItems: AdminMenuItem[] = [
    { Name: 'Пользователи', Link: 'users' },
    { Name: 'Расписание', Link: 'foodTimes' },
    { Name: 'Записи', Link: 'foodRecordings' },
    { Name: 'Группы', Link: 'groups' },
  ];

  constructor() {}

  setFixedNavBar(value: boolean): void {
    localStorage.setItem(this.FIXED_NAVBAR_KEY, value.toString());
  }
}
