import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Group } from '../models/Group';
import { map } from 'rxjs/operators';
import { User } from '../models/User';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  private API_ROOT: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private authorization: AuthorizationService
  ) {}

  getGroups(): Observable<Group[]> {
    return this.http.get(`${this.API_ROOT}Group`).pipe(
      map((data: any) => {
        return data.map((group: any): Group => {
          return new Group(group.id, group.name, group.count);
        });
      })
    );
  }

  getGroupUser() {
    return this.http.get(
      `${this.API_ROOT}GroupUsers/user`,
      this.authorization.getHeaders()
    );
  }

  getGroupUsers(groupId: any) {
    return this.http.get(`${this.API_ROOT}Group/users?groupId=${groupId}`);
  }

  createGroup(name: string) {
    return this.http.post(`${this.API_ROOT}Group`, {
      name: name,
    });
  }

  deleteGroup(id: any): Observable<object> {
    return this.http.delete(
      `${this.API_ROOT}Group/${id}`,
      this.authorization.getHeaders()
    );
  }

  addGroupUser(userId: string, groupId: number) {
    return this.http.post(
      `${this.API_ROOT}GroupUsers?userId=${userId}&groupId=${groupId}`,
      {}
    );
  }
}
