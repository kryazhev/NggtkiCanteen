import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterUser } from '../models/RegisterUser';
import { environment } from 'src/environments/environment';
import { LoginUser } from '../models/LoginUser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationService {
  private API_ROOT: string = environment.apiUrl;
  private tokenName = 'auth_token';
  private IdName = 'auth_id';
  private idName = 'auth_id';

  constructor(private http: HttpClient, private router: Router) {}

  login(user: LoginUser): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    this.http
      .post(this.API_ROOT + 'User/signIn', JSON.stringify(user), httpOptions)
      .subscribe(
        (data: any) => {
          localStorage.setItem(this.tokenName, data.token);
          localStorage.setItem(this.idName, data.id);
          this.router.navigate(['']);
        },
        (error) => console.log(error)
      );
  }

  register(user: RegisterUser): Observable<Object> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.http.post(
      this.API_ROOT + 'User',
      JSON.stringify(user),
      httpOptions
    );
  }

  logout(): void {
    localStorage.removeItem(this.tokenName);
    localStorage.removeItem(this.IdName);
    this.router.navigate(['']);
  }

  isAuthorization(): boolean {
    return localStorage.getItem(this.tokenName) ? true : false;
  }

  getToken(): string | null {
    return localStorage.getItem(this.tokenName);
  }

  getId() {
    return localStorage.getItem(this.idName);
  }

  getHeaders(): object {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.getToken()}`,
      }),
    };
  }

  getAccount(): Observable<object> {
    return this.http.get(this.API_ROOT + `User/account`, this.getHeaders());
  }

  getUsers(): Observable<object> {
    return this.http.get(`${this.API_ROOT}User`, this.getHeaders());
  }

  getUsersIsGroup(): Observable<object> {
    return this.http.get(`${this.API_ROOT}User/isGroups`, this.getHeaders());
  }

  userIsGroup() {
    return this.http.get(`${this.API_ROOT}User/userIsGroup`, this.getHeaders());
  }

  deleteUser(id: string) {
    return this.http.delete(
      this.API_ROOT + `User?userid=${id}`,
      this.getHeaders()
    );
  }
}
