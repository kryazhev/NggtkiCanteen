import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FoodTimeCreate } from '../models/FoodTimeCreate';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class FoodTimeService {
  private API_ROOT: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private authorization: AuthorizationService
  ) {}

  getFoodTimes() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get(`${this.API_ROOT}FoodTime`, httpOptions);
  }

  createFoodTime(foodTimeCreate: FoodTimeCreate) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.authorization.getToken()}`,
      }),
    };

    return this.http.post(
      `${this.API_ROOT}FoodTime`,
      foodTimeCreate,
      httpOptions
    );
  }

  deleteFoodTime(id: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.authorization.getToken()}`,
      }),
    };

    return this.http.delete(`${this.API_ROOT}FoodTime/${id}`, httpOptions);
  }
}
