import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  user: any;
  isLoader = true;
  isChangePassword = true;
  token: string | null = '';

  constructor(private authorizationService: AuthorizationService) {}

  ngOnInit(): void {
    this.authorizationService.getAccount().subscribe((data) => {
      this.user = data;
      this.isLoader = false;
    });

    if (
      environment.production === false &&
      this.authorizationService.getToken() != null
    ) {
      this.token = this.authorizationService.getToken();
    }
  }
}
