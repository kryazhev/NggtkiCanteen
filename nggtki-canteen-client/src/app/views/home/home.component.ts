import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterUser } from 'src/app/models/RegisterUser';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [AuthorizationService],
})
export class HomeComponent implements OnInit {
  isAuthorization = false;

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router,
    private global: GlobalService
  ) {}

  ngOnInit(): void {
    this.isAuthorization = this.authorizationService.isAuthorization();
  }

  register(user: RegisterUser): void {
    const isRegister = this.authorizationService.register(user);
    isRegister.subscribe((data: any) => {
      alert(`Пользователь ${data.userName} зарегистрирован`);
      this.router.navigate(['']);
    });
  }
}
