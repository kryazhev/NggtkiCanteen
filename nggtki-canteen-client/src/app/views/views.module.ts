import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthorizationService } from '../services/authorization.service';
import { AccountComponent } from './account/account.component';
import { FoodTimeService } from '../services/food-time.service';
import { AppFormsModule } from '../app-forms/app-forms.module';

@NgModule({
  declarations: [HomeComponent, AccountComponent],
  providers: [AuthorizationService, FoodTimeService],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, AppFormsModule],
})
export class ViewsModule {}
