import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { GroupService } from 'src/app/services/group.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  user: any;
  isLoader = true;
  isChangePassword = true;
  token: string | null = '';
  group: any;

  constructor(
    private authorizationService: AuthorizationService,
    private groupService: GroupService
  ) {}

  ngOnInit(): void {
    this.authorizationService.getAccount().subscribe((data) => {
      this.user = data;
      this.isLoader = false;
    });

    this.groupService.getGroupUser().subscribe((data: any) => {
      this.group = data;
    });

    if (
      environment.production === false &&
      this.authorizationService.getToken() != null
    ) {
      this.token = this.authorizationService.getToken();
    }
  }
}
