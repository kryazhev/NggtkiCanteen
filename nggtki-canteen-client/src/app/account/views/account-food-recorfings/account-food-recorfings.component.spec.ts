import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountFoodRecorfingsComponent } from './account-food-recorfings.component';

describe('AccountFoodRecorfingsComponent', () => {
  let component: AccountFoodRecorfingsComponent;
  let fixture: ComponentFixture<AccountFoodRecorfingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountFoodRecorfingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFoodRecorfingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
