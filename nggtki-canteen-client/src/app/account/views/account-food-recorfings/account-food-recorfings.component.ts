import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { FoodRecordingCreate } from 'src/app/models/FoodRecordingCreate';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { FoodRecprdingsService } from 'src/app/services/food-recprdings.service';
import { FoodTimeService } from 'src/app/services/food-time.service';

@Component({
  selector: 'app-account-food-recorfings',
  templateUrl: './account-food-recorfings.component.html',
  styleUrls: ['./account-food-recorfings.component.scss'],
})
export class AccountFoodRecorfingsComponent implements OnInit {
  userFoodRecordings: any[] = [];
  isModal = false;
  foodRecordingForm: FormGroup;
  foodTimes: any[] = [];
  isGroup = false;

  constructor(
    private foodRecording: FoodRecprdingsService,
    private formBuilder: FormBuilder,
    private foodTime: FoodTimeService,
    private auth: AuthorizationService
  ) {
    this.foodRecordingForm = this.formBuilder.group({
      foodTime: new FormControl('Выберите приём пищи', Validators.required),
      date: new FormControl(null, Validators.required),
    });
  }

  loadFoodRecording() {
    this.foodRecording.getFoodRecordings().subscribe((data: any) => {
      this.userFoodRecordings = data;
      console.log(data);
    });
  }

  ngOnInit(): void {
    this.loadFoodRecording();

    this.foodTime.getFoodTimes().subscribe((data: any) => {
      this.foodTimes = data;
    });

    this.auth.userIsGroup().subscribe((data: any) => {
      this.isGroup = data;
    });
  }

  createFoodRecording() {
    if (this.auth.getId() != null) {
      this.foodRecording
        .createFoodRecordings(
          new FoodRecordingCreate(
            this.foodRecordingForm.get('foodTime')?.value,
            this.auth.getId() ?? '',
            this.foodRecordingForm.get('date')?.value
          )
        )
        .subscribe((data: any) => {
          this.loadFoodRecording();
          this.isModal = false;
          alert('Вы записались');
        });
    }
  }

  deleteFoodRecording(id: any) {
    this.foodRecording.deleteFoodRecording(id).subscribe(() => {
      this.loadFoodRecording();
    });
  }
}
