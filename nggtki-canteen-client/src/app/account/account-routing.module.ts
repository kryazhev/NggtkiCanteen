import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizationGuard } from '../authorization/AuthorizationGuard';
import { AccountFoodRecorfingsComponent } from './views/account-food-recorfings/account-food-recorfings.component';
import { AccountComponent } from './views/account/account.component';
import { IndexComponent } from './views/index/index.component';

const routes: Routes = [
  {
    path: 'account',
    canActivate: [AuthorizationGuard],
    component: IndexComponent,
    children: [
      {
        path: '',
        component: AccountComponent,
      },
      {
        path: 'foodRecording',
        component: AccountFoodRecorfingsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
