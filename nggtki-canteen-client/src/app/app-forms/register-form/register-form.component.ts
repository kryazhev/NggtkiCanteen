import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { RegisterUser } from 'src/app/models/RegisterUser';

function passwordRepeating(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('password');
  const cpwd = c.parent.get('repeatPassword');

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };
  }
}

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  registerForm: FormGroup;

  @Output()
  onRegistered = new EventEmitter<RegisterUser>();

  constructor(private formBuilder: FormBuilder) {
    this.registerForm = this.formBuilder.group({
      name: new FormControl(null, Validators.required),
      surname: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, Validators.required),
      repeatPassword: new FormControl(null, [
        Validators.required,
        passwordRepeating,
      ]),
    });
  }

  ngOnInit(): void {}

  register(): void {
    this.onRegistered.emit(
      new RegisterUser(
        this.registerForm.get('name')?.value +
          '-' +
          this.registerForm.get('surname')?.value,
        this.registerForm.get('email')?.value,
        this.registerForm.get('password')?.value
      )
    );
  }
}
