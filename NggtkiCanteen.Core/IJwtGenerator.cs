﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NggtkiCanteen.Core
{
    public interface IJwtGenerator<T>
    {
        string CreateToken(T user);
    }
}
