﻿using Microsoft.AspNetCore.Mvc;
using NggtkiCanteen.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NggtkiCanteen.Core
{
    public interface IRepository<T,C> where T : BaseModel
    {
        Task<IEnumerable<T>> All();
        Task Create(C create);
        Task<T> Update(int id, T value);
        Task Delete(int id);
    }
}
