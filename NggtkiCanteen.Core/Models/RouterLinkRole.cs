﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NggtkiCanteen.Core.Models
{
    public class RouterLinkRole : BaseModel
    {
        public string Route { get; set; }
        public string Role { get; set; }
    }
}
