﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NggtkiCanteen.Core.Models
{
    public class FoodTime : BaseModel
    {
        public string Name { get; set; }
        public TimeSpan Time { get; set; }
    }
}
