﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NggtkiCanteen.Core.Models
{
    public class FoodRecording : BaseModel
    {
        public string UserId { get; set; }
        public User User { get; set; }

        public int FoodTimeId { get; set; }
        public FoodTime FoodTime { get; set; }

        public DateTime Date { get; set; }
        public bool Approved { get; set; }
    }
}
