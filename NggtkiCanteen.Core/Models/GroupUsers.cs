﻿using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NggtkiCanteen.Core.Models
{
    public class GroupUsers : BaseModel
    {
        public int GroupsId { get; set; }
        public Group Groups { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }
        
    }
}
